# Portfolio

This portfolio website was developed for educational purposes, showcasing the collaborative spirit of the programming and learning community.


## Features

***Here are 7 features of this portfolio website***
- **Home Page** : *Profile Image, Official Profile Name, Highlighting Skills, Quick Navigation*
- **Journey Page** : *Showing Resume of Experience and Education.*
- **Portfolio Page** : *Content Sharing*
- **Contact Page** : *Address, Profile Social Media, Email, Number Phone, Sending Message Box.*
- **Download Page** : *link to google playstore*
- **About Page** : *Describe Personal Information*
- **Privacy Policy Page** : *Policy Information*- Awesome Readme Templates



**Home Page 👇**

![App Screenshot](https://github.com/TheTevea/portfolio-template/blob/master/assets/screenshot/home.png?raw=true)


**Journey Page 👇**

![App Screenshot](https://github.com/TheTevea/portfolio-template/blob/master/assets/screenshot/journey.png?raw=true)


**Portfolio Page 👇**

![App Screenshot](https://github.com/TheTevea/portfolio-template/blob/master/assets/screenshot/portfolio.png?raw=true)


**Contact Page 👇**

![App Screenshot](https://github.com/TheTevea/portfolio-template/blob/master/assets/screenshot/contactus.png?raw=true)

*Download Button* ( only link into google playstore)


**About Page 👇**

![App Screenshot](https://github.com/TheTevea/portfolio-template/blob/master/assets/screenshot/about.png?raw=true)


**Privacy Policy 👇**

![App Screenshot](https://github.com/TheTevea/portfolio-template/blob/master/assets/screenshot/privacy%20policy.png?raw=true)


*All intellectual property rights and credits for the template go to **Sochea Official**.*



